package com.example.demo.controller;

import com.example.demo.message.ResponseMessage;
import com.example.demo.message.ResumeDto;
import com.example.demo.service.FilesStorageServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("http://localhost:8081")
@RequestMapping(name = "/files")
@RequiredArgsConstructor
public class ResumeController {

  private final FilesStorageServiceImpl storageService;

  @PostMapping("/upload")
  public ResponseEntity<ResponseMessage> uploadFile(ResumeDto resumeDto) { //TODO добавить парметр для получения данных резюме с файлами

    System.out.println("RESUME: "+resumeDto);

    String message = "";
    try {
      storageService.save();

      message = "Uploaded the file successfully";
      return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
    } catch (Exception e) {
      message = "Could not upload the file: Error: " + e.getMessage();
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
    }
  }

  @GetMapping("/id")
  public ResponseEntity<Resource> getFile(@RequestParam Long id) {
    Resource file = storageService.getFile(id);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
  }

  @DeleteMapping("/id")
  public ResponseEntity<ResponseMessage> deleteFile(@RequestParam Long id) {
    String message = "";

    try {
      boolean existed = storageService.delete(id);

      if (existed) {
        message = "Delete the file successfully";
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
      }

      message = "The file does not exist!";
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    } catch (Exception e) {
      message = "Could not delete the file: Error: " + e.getMessage();
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseMessage(message));
    }
  }
}

package com.example.demo.message;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class ResumeDto {

    String title;
    String name;
    List<MultipartFile> multipartFiles;
}

package com.example.demo.service;



import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@RequiredArgsConstructor
@Service
public class FilesStorageServiceImpl implements FilesStorageService {

    private final ModelMapper modelMapper;
    private final Path root = Paths.get(""); //TODO указать путь к папке для сохранения файлов

    @Override
    //TODO добавить аннотацию которая откатит изменения в случае ошибки
    public void save() {  //TODO добавить парметр для получения данных резюме с файлами
//        try {
//            //TODO сохранить резюме в базу
//            //TODO сохранить файлы на компьютере и добавить в базу с привязкой к сохраненному резюме (в цикле)
//
//            Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename())); //TODO фрагмент для сохранения на компьютере
//
//        } catch (Exception e) {
//            if (e instanceof FileAlreadyExistsException) {
//                throw new kz.company.task.exception.GeneralException(ApiErrorType.E404_INCORRECT_FILE_URL.toString());
//            }
//
//            throw new RuntimeException(e.getMessage());
//        }
    }

    @Override
    public Resource getFile(Long id) {
        try {

            Path file = root.resolve(""); //TODO вытащить файл по id, метод принимает имя файла
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public boolean delete(Long id) {
//        try {
//            Path file = root.resolve(filename); //TODO вытащить файл по id, метод принимает имя файла
//            return Files.deleteIfExists(file);
//        } catch (IOException e) {
//            throw new RuntimeException("Error: " + e.getMessage());
//        }
        return true;
    }

}

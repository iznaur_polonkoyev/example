package com.example.demo.service;


import org.springframework.core.io.Resource;

public interface FilesStorageService {

    void save();

    Resource getFile(Long id);

    boolean delete(Long id);
}
